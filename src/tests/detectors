#! /bin/sh

# Copyright (C) 2010 Colin Watson.
#
# This file is part of binfmt-support.
#
# binfmt-support is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# binfmt-support is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with binfmt-support; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# Test userspace detectors.

: "${srcdir=.}"
# shellcheck source-path=SCRIPTDIR
. "$srcdir/testlib.sh"

init
fake_proc

for i in 1 2 3; do
	cat >"$tmpdir/program-$i" <<EOF
#! /bin/sh
echo program-$i "\$@"
EOF
	chmod +x "$tmpdir/program-$i"
done

for i in 1 2; do
	cat >"$tmpdir/detector-$i" <<EOF
#! /bin/sh
grep -q ^$i "\$1"
EOF
	chmod +x "$tmpdir/detector-$i"
done

for i in 1 2 3; do
	cat >"$tmpdir/input-$i.ext" <<EOF
$i input file
EOF
done

update_binfmts_proc \
	--install test-1 "$tmpdir/program-1" --extension ext \
	--detector "$tmpdir/detector-1"
report 'detector 1: install' "$?"
cat >"$tmpdir/1-admin.exp" <<EOF
:
extension
0
ext

$tmpdir/program-1
$tmpdir/detector-1



EOF
expect_files_equal 'detector 1: admindir entry OK' \
	"$tmpdir/var/lib/binfmts/test-1" "$tmpdir/1-admin.exp"
# shellcheck disable=SC2154
cat >"$tmpdir/1-proc.exp" <<EOF
enabled
interpreter $pkglibexecdir/run-detectors
flags: 
extension .ext
EOF
expect_files_equal 'detector 1: procdir entry OK' \
	"$tmpdir/proc/test-1" "$tmpdir/1-proc.exp"

update_binfmts_proc \
	--install test-2 "$tmpdir/program-2" --extension ext \
	--detector "$tmpdir/detector-2"
report 'detector 2: install' "$?"
cat >"$tmpdir/2-admin.exp" <<EOF
:
extension
0
ext

$tmpdir/program-2
$tmpdir/detector-2



EOF
expect_files_equal 'detector 2: admindir entry OK' \
	"$tmpdir/var/lib/binfmts/test-2" "$tmpdir/2-admin.exp"
cat >"$tmpdir/2-proc.exp" <<EOF
enabled
interpreter $pkglibexecdir/run-detectors
flags: 
extension .ext
EOF
expect_files_equal 'detector 2: procdir entry OK' \
	"$tmpdir/proc/test-2" "$tmpdir/2-proc.exp"

update_binfmts_proc --install test-3 "$tmpdir/program-3" --extension ext
report 'no detector: install' "$?"
cat >"$tmpdir/3-admin.exp" <<EOF
:
extension
0
ext

$tmpdir/program-3




EOF
expect_files_equal 'no detector: admindir entry OK' \
	"$tmpdir/var/lib/binfmts/test-3" "$tmpdir/3-admin.exp"
cat >"$tmpdir/3-proc.exp" <<EOF
enabled
interpreter $pkglibexecdir/run-detectors
flags: 
extension .ext
EOF
expect_files_equal 'no detector: procdir entry OK' \
	"$tmpdir/proc/test-3" "$tmpdir/3-proc.exp"

echo "program-1 $tmpdir/input-1.ext --foo=bar file --admindir=baz -v" >"$tmpdir/4.exp"
run_detectors "$tmpdir/input-1.ext" --foo=bar file --admindir=baz -v >"$tmpdir/4.out"
report 'detector 1: run' "$?"
expect_files_equal 'detector 1: output' \
	"$tmpdir/4.out" "$tmpdir/4.exp"

echo "program-2 $tmpdir/input-2.ext" >"$tmpdir/5.exp"
run_detectors "$tmpdir/input-2.ext" >"$tmpdir/5.out"
report 'detector 2, without arguments: run' "$?"
expect_files_equal 'detector 2, without arguments: output' \
	"$tmpdir/5.out" "$tmpdir/5.exp"

echo "program-3 $tmpdir/input-3.ext foo --arg" >"$tmpdir/6.exp"
run_detectors "$tmpdir/input-3.ext" foo --arg >"$tmpdir/6.out"
report 'no detector: run' "$?"
expect_files_equal 'no detector: output' \
	"$tmpdir/6.out" "$tmpdir/6.exp"

echo "program-3 $tmpdir/input-3.ext" >"$tmpdir/7.exp"
run_detectors "$tmpdir/input-3.ext" >"$tmpdir/7.out"
report 'no detector, without arguments: run' "$?"
expect_files_equal 'no detector, without arguments: output' \
	"$tmpdir/7.out" "$tmpdir/7.exp"

finish
